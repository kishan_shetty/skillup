package com.example.skillup.ui.main.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.example.skillup.R
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    private val SPLASH_TIME: Long = 1000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        splashAnimation()
        Handler(Looper.getMainLooper())
            .postDelayed(
                { launchMainActivity() },
                SPLASH_TIME
            )
    }

    private fun launchMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun splashAnimation() {
        val animZoomIn = AnimationUtils.loadAnimation(applicationContext, R.anim.text_zoom_in)
        tvWelcome.startAnimation(animZoomIn)
    }
}