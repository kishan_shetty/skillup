package com.example.skillup.ui

import android.view.View
import android.view.ViewGroup
import com.example.skillup.data.modle.TopicsModel
import com.example.skillup.ui.base.BaseViewHolder
import com.example.skillup.ui.main.listener.TopicClickListener
import kotlinx.android.synthetic.main.item_topic.view.*

class TopicItemViewHolder(layoutId: Int, parent: ViewGroup, private val listener: TopicClickListener) :
    BaseViewHolder<TopicsModel>(layoutId, parent) {

    override fun bindData(pos: Int, model: TopicsModel, itemView: View) {
        itemView.apply {
            tvTopic.text= model.topicName

            tvTopic.setOnClickListener {
                listener.onTopicClick(adapterPosition)
            }
        }
    }
}