package com.example.skillup.ui.main.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.example.skillup.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        launchTopicsFragment()
    }

    fun launchTopicsFragment() {
        val fragment: TopicFragment = TopicFragment()
        fragment.setArguments(Bundle())
        val transaction: FragmentTransaction =
            this.getSupportFragmentManager().beginTransaction()
        transaction.add(R.id.fl_container, fragment, TAG)
        transaction.commit()
    }
}