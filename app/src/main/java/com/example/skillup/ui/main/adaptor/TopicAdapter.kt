package com.example.skillup.ui.main.adaptor

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.skillup.R
import com.example.skillup.data.modle.TopicsModel
import com.example.skillup.ui.TopicItemViewHolder
import com.example.skillup.ui.base.BaseViewHolder
import com.example.skillup.ui.main.listener.TopicClickListener
import kotlinx.android.synthetic.main.item_topic.view.*

class TopicAdapter (private val itemList: List<TopicsModel>,
                                               private val listener: TopicClickListener, lis: List<TopicsModel>
) :
        RecyclerView.Adapter<BaseViewHolder<Any>>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
            return TopicItemViewHolder(R.layout.item_topic, parent, listener) as BaseViewHolder<Any>
        }
        override fun onBindViewHolder(holder: BaseViewHolder<Any>, position: Int) {
            holder.bindData(position, itemList[position], holder.itemView)
        }

        override fun getItemCount(): Int {
            return itemList.size
        }
}