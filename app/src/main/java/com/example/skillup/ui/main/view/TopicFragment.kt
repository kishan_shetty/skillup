package com.example.skillup.ui.main.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.skillup.R
import com.example.skillup.data.modle.TopicsModel
import com.example.skillup.ui.main.adaptor.TopicAdapter
import com.example.skillup.ui.main.listener.TopicClickListener
import kotlinx.android.synthetic.main.fragment_topic.*

class TopicFragment : Fragment(), TopicClickListener{

    fun newInstance(bundle: Bundle?): TopicFragment? {
        val fragment: TopicFragment = TopicFragment()
        fragment.setArguments(bundle)
        return fragment
    }
    private lateinit var adapter: TopicAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_topic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerViewAdapter(getTopic())

    }

    private fun getTopic():List<TopicsModel> {
        return listOf(TopicsModel("Storts"), TopicsModel("Enter"))
    }

    fun initRecyclerViewAdapter(adapterList: List<TopicsModel>) {
        adapter = TopicAdapter(adapterList, this, getTopic())
        rvTopicList.adapter = adapter
        rvTopicList.layoutManager = GridLayoutManager(
            requireContext(),
            2,
            RecyclerView.VERTICAL,
            false
        )
    }

    override fun onTopicClick(adapterPosition: Int) {
        Toast.makeText(activity, "this is toast message", Toast.LENGTH_SHORT).show()
    }

}