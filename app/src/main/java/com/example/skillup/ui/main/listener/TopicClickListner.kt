package com.example.skillup.ui.main.listener

interface TopicClickListener {

    fun onTopicClick(adapterPosition: Int)

}